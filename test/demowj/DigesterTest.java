package demowj;


import demowj.domain.Student;
import demowj.domain.Subjects;
import org.apache.tomcat.util.digester.Digester;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * @author wujun
 * @description
 * @date 19/5/23.
 */
public class DigesterTest {
    private Student student;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public static void main(String[] args) {
        DigesterTest digesterTest = new DigesterTest();
        digesterTest = digesterTest.parseDigesterXml();

        Iterator<Subjects> it = digesterTest.getStudent().getList().iterator();
        while ( it.hasNext()) {
            it.next().print();
        }
    }

    private DigesterTest parseDigesterXml(){
        String xmlPath = DigesterTest.class.getResource("").getPath();
        File xml = new File(xmlPath + "/digester.xml");
        Digester digester = new Digester();

        digester.addObjectCreate( "student" , "demowj.domain.Student" );
        digester.addSetProperties("student");
        digester.addObjectCreate("student/subjects" , "demowj.domain.Subjects");
        digester.addSetProperties("student/subjects");
        digester.addSetNext("student/subjects" , "add", "demowj.domain.Subjects");
        digester.push(this);
        digester.addSetNext("student" , "setStudent", "demowj.domain.Student");

        DigesterTest digesterTest = null;
        try {
            digesterTest = (DigesterTest)digester.parse(xml);
        }catch ( SAXException e ){
            System.out.println("parse with mistakes!");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return digesterTest;

    }
}
